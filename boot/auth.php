<?php

    class Auth{

        public static function set($id, $name, $email, $rule){
            $_SESSION['userData']['id'] = $id;
            $_SESSION['userData']['name'] = $name;
            $_SESSION['userData']['email'] = $email;
            $_SESSION['userData']['rule'] = $rule;
        }

        public static function isGuest(){
            if(count($_SESSION['userData']) != 0){
                return false;
            }
            return true;
        }

        public static function isPR(){
            if($_SESSION['userData']['rule'] === "PR"){
                return true;
            }
            return false;
        }

        public static function reset(){
            $_SESSION['userData'] = [];
        }

    }

?>