<?php

    class Boot{
        public function getCurrentUrl($config) {
            $currentUrl = $_SERVER['REQUEST_URI'];
            $explodedUrl = explode($config['PROJECT_NAME'], $currentUrl);

            return $explodedUrl[1];
        }
    }

?>