<?php
    include('boot/util.php');

    class Controller extends Util{

        public $layout;
        public $title;

        public function view($path, $data = []){ 
            $content = 'view/'.$path.'.php'; 
            return include('view/layout/'.$this->layout.'.php');
        }

    }

?>