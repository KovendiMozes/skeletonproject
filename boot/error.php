<?php

    class ErrorManager{

        public static function isError(){
            if(count($_SESSION['errors']) != 0){
                return true;
            }
            return false;
        }

        public static function getErrors(){
            $html = '<ul>';
            foreach($_SESSION['errors'] as $error){
                $html .= '<li>'.$error.'</li>';
            }
            $html .= '</ul>';
            echo $html;
        }

        public static function reset(){
            $_SESSION['errors'] = [];
        }

        public static function setError($key, $massage){
            $_SESSION['errors'][$key] = $massage;
        }
    }

?>