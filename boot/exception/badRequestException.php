<?php

    class BadRequestException extends Exception{ }

    function BadRequestException($methodType){
        if($_SERVER['REQUEST_METHOD'] != $methodType){
            throw new BadRequestException("The request method is bad!");
        }
    }

?>