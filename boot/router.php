<?php
    include('boot/boot.php');
    include('boot/exception/exception.php');

    class Router extends Boot{

        private $currentUrl;

        private $routerList;

        private $targetRequestMethod;

        private $targetRequestUrl;

        private $targetRequest;

        public function __construct($router, $config){
            $this->routerList = $router;
            $this->currentUrl = $this->getCurrentUrl($config);
            $this->routerValidation();
        }

        private function routerValidation(){

            foreach($this->routerList as $route){
                $this->targetRequestMethod = $route[0];
                $this->targetRequestUrl = $route[1];
                if($this->urlValidation() == True && $this->methodValidation() == True){
                    $this->targetRequest = $route[2];
                    break;
                }
            }
            $this->exceptionValidation();
            
            $this->classFinder();
        }


        private function exceptionValidation(){
            //var_dump($this->targetRequestMethod.'method');
            //var_dump($this->targetRequestUrl.'url');
            if($this->urlValidation() != True){
                $this->exceptionUrl();
            }
            else if($this->methodValidation() != True){
                $this->expectedMethod($this->targetRequestMethod);
            }
        }

        private function urlValidation(){
            if($this->targetRequestUrl == $this->currentUrl){
                return True;
            }
        }

        private function methodValidation(){
            if($this->targetRequestMethod == $_SERVER['REQUEST_METHOD']){
                return True;
            }
        }

        private function expectedMethod($targetRequestMethod){
            try {
                BadRequestException($targetRequestMethod);
            } catch (BadRequestException $e){
                echo $e->getMessage();
                exit();
            }
        }

        private function exceptionUrl(){
            try {
                BadUrlException();
            } catch (BadUrlException $e){
                echo $e->getMessage();
                exit();
            }
        }

        private function classFinder(){
            $explodeTargetRequest = explode('@', $this->targetRequest);
            $className = $explodeTargetRequest[0];
            $methodName = $explodeTargetRequest[1];

            include('controller/'.$className.'.php');
            $class = new $className();
            $this->methodFinder($class, $methodName);
        }

        private function methodFinder($class, $methodName){
            if ($this->targetRequestMethod == 'POST' || $this->targetRequestMethod == 'PUT'){
                return $class->$methodName($_REQUEST);
            } 
            $class->$methodName();
        }

    }

?>