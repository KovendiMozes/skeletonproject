<?php

    include('boot/controller.php');
    include('boot/redirect.php');
    include('service/event.php');

    class Event extends Controller{

        public function __construct(){
            $this->layout = 'index';
        }

        public function eventPage(){
            $this->title = 'Event Page Title';
            return  $this->view('event/event', [
            ]);
        }

        public function eventValidation(){
            $eventService = new EventService();

            $eventV = $eventService->insertEvent($_POST['name']);

            header(Redirect::to($this->baseUrl.'/event'));
        }

    }

?>