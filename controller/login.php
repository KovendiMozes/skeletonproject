<?php

    include('boot/controller.php');
    include('service/authentication.php');
    include('boot/redirect.php');

    class Login extends Controller{

        public function __construct(){
            $this->layout = 'login';
            Auth::reset();
        }

        public function loginValidation(){
            //var_dump($_POST['uname']);
            $authentication = new AuthenticationService();

            $isValid = $authentication->loginAuthentication($_POST['uname'], $_POST['psw']);

            if(!$isValid){
                return Redirect::to($this->baseUrl.'/login');
            }

            $authentication->uploadSession($_POST['uname']);
            
            Redirect::to($this->baseUrl.'/home');
        }


        public function loginPage(){
            $this->title = 'Login Page Title';
            return  $this->view('login/login', [
            ]);
        }

    }

?>