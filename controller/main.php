<?php

    include('boot/controller.php');
    include('service/home.php');

    class Main extends Controller{

        public function __construct(){
            $this->layout = 'index';
        }

        public function mainPage(){
            $homeSercice = new HomeService();
            $this->title = 'Home Page Title';
            return  $this->view('main/main', [
                'homeServiceI' => $homeSercice->allImage('Screen')
            ]);
        }

        public function allImageWithEvent(){
            $homeSercice = new HomeService();
            $this->title = 'Home Page Title';
            return  $this->view('main/main', [
                'homeServiceI' => $homeSercice->allImage($_POST['event'])
            ]);
        }

    }

?>