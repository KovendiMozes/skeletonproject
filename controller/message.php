<?php

    include('boot/controller.php');
    include('service/message.php');
    include('boot/redirect.php');

    class Message extends Controller{

        public $menu;

        public function __construct(){
            $this->layout = 'index';
        }

        public function sendMessage(){
            $messageService = new MessageService();

            $messageValid = $messageService->validMessage($_POST['address'], $_POST['title'], $_POST['description'], $_SESSION['userData']['id']);

            if(!$messageValid){
                header(Redirect::to($this->baseUrl.'/send-message'));
            }
            else{
                header(Redirect::to($this->baseUrl.'/message'));
            }
        }

        public function messagePage(){
            $messageService = new MessageService();
            $this->title = 'Message Page Title';
            $this->menu = 'messages';
            return  $this->view('message/message', [
                'messageService' => $messageService->allMessage()
            ]);
        }

        public function sendMessagePage(){
            $this->title = 'Message Page Title';
            $this->menu = 'sendMessage';
            return  $this->view('message/message', [
                
            ]);
        }

    }

?>