<?php

    include('boot/controller.php');
    include('service/profile.php');

    class Profile extends Controller{

        public $menu;

        public function __construct(){
            $this->layout = 'index';
        }

        public function profilePage(){
            $profileService = new ProfileService();
            $this->title = 'Profile Page Title';
            $this->menu = 'profileImage';
            return  $this->view('profile/profile', [
                'profileSercice' => $profileService->allImage()
            ]);
        }

        public function profileSettingsPage(){
            $this->title = 'Profile Page Title';
            $this->menu = 'profileSettings';
            return  $this->view('profile/profile', [
                
            ]);
        }

    }

?>