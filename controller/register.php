<?php

    include('boot/controller.php');
    include('service/register.php');
    include('boot/redirect.php');

    class Register extends Controller{

        public function __construct(){
            $this->layout = 'register';
        }

        public function registerValidation(){
            $registerService = new RegisterService();

            $registV = $registerService->registerValid($_POST['email'], $_POST['uname'], $_POST['psw'], $_POST['rpsw']);

            if($registV == false){
                header(Redirect::to($this->baseUrl.'/register'));
            }
            else{
                header(Redirect::to($this->baseUrl.'/login'));
            }

        }

        public function registerPage(){
            $this->title = 'Register Page Title';
            return  $this->view('register/register', [
                
            ]);
        }

    }

?>