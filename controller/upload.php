<?php

    include('boot/controller.php');
    include('service/uploadImage.php');
    include('boot/redirect.php');

    class Upload extends Controller{

        public function __construct(){
            $this->layout = 'index';
        }

        public function uploadValidation(){
            $uploadImage = new UploadImage();

            $isValid = $uploadImage->uploadValidationService($_POST['title'], $_POST['description'], $_POST['event'], $_POST['whoCanSee'], $_FILES['image'], $_SESSION['userData']['id'], $_POST['permission']);

            if(!$isValid){
                header(Redirect::to($this->baseUrl.'/upload'));
            }

            header(Redirect::to($this->baseUrl.'/upload'));
        }

        public function uploadPage(){
            $this->title = 'Upload Page Title';
            return  $this->view('upload/upload', [
                
            ]);
        }

    }

?>