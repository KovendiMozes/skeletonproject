<?php

    include('boot/controller.php');
    include('service/user.php');

    class User extends Controller{

        public function __construct(){
            $this->layout = 'index';
        }

        public function userPagePost(){
            $userService = new UserService();
            //var_dump('POST');
            return  $this->view('user/user', [
                'homeService' => $userService->InsertUser()
            ]);
        }

        public function userPageGet(){
            $this->title = 'User Page Title';
            $userService = new UserService();

            return  $this->view('user/user', [
                'homeService' => $userService->AllUsers()
            ]);
        }

        public function userPagePut(){
            $userService = new UserService();
            //var_dump('PUT');
            return  $this->view('user/user', [
                'homeService' => $userService->UpdateUser()
            ]);
        }

    }

?>