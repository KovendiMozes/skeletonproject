<?php

    include('boot/config.php');
    include('boot/router.php');

    $router = [
        ['POST', '/user', 'user@userPagePost'],
        ['GET', '/user', 'user@userPageGet'],
        ['PUT', '/user', 'user@userPagePut'],

        ['GET', '/home', 'main@mainPage'],
        ['POST', '/home', 'main@allImageWithEvent'],

        ['GET', '/login', 'login@loginPage'],
        ['POST', '/login', 'login@loginValidation'],

        ['GET', '/register', 'register@registerPage'],
        ['POST', '/register', 'register@registerValidation'],

        ['GET', '/upload', 'upload@uploadPage'],
        ['POST', '/upload', 'upload@uploadValidation'],

        ['GET', '/profile', 'profile@profilePage'],
        ['GET', '/profile-settings', 'profile@profileSettingsPage'],

        ['GET', '/message', 'message@messagePage'],
        ['POST', '/message', 'message@sendMessage'],
        ['GET', '/send-message', 'message@sendMessagePage'],
        
        ['GET', '/event', 'event@eventPage'],
        ['POST', '/event', 'event@eventValidation']
    ];

    new Router($router, $config);

?>