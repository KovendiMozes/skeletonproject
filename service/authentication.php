<?php

include('boot/databaseconnection.php');

    class AuthenticationService extends Db{


        public function loginAuthentication($uname, $psw){
            $isError = false;

            if(empty($uname)){
                ErrorManager::setError('usernameEmpty', 'Username is empty!');
                $isError = true;
            }

            if(empty($psw)){
                ErrorManager::setError('passwordEmpty', 'Password is empty!');
                $isError = true;
            }

            if($isError == true){
                return false;
            }
            
            $db = $this->databaseConnection();

            $sql = 'SELECT name, password FROM user where name = "'.$uname.'"';
            $result = $db -> query($sql);

            $row = $result -> fetch_assoc();
            
            if($row == NULL || md5($psw) != $row['password']){
                ErrorManager::setError('userNotFound', 'Username or password is wrong!');
                return false;
            }

            return true;
        }


        public function uploadSession($uname){
            $db = $this->databaseConnection();

            $sql = 'SELECT id ,name, email, rule FROM user where name = "'.$uname.'"';
            $result = $db -> query($sql);

            $row = $result -> fetch_assoc();
            
            Auth::set($row['id'], $row['name'], $row['email'], $row['rule']);
        }

    }

?>