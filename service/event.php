<?php

    include('boot/databaseconnection.php');

    class EventService extends Db{

        public function insertEvent($name){

            if(empty($name)){
                ErrorManager::setError('nameEmpty', 'Name is empty!');
                return false;
            }

            $db = $this->databaseConnection();

            $date = date("Y-m-d h:i:s");
            $id = intval($_SESSION['userData']['id']);

            $sql = 'INSERT INTO event (name, user, createdAt, updatedAt) VALUES ("'.$name.'", '.$id.', "'.$date.'", "'.$date.'")';
            
            if (mysqli_query($db, $sql)) {

            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($db);
                return false;
            }

            return true;

        }

    }

?>