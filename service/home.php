<?php

    include('boot/databaseconnection.php');

    class HomeService extends Db{

        public function allImage($event){
            $db = $this->databaseConnection();

            if(Auth::isGuest()==true){
                $sql = 'SELECT createdAt, title, description, name, user FROM uploadImage';
            }
            else{
                $sql = 'SELECT createdAt, title, description, name, user FROM uploadImage where event = "'.$event.'"';
            }
            
            $result = $db -> query($sql);

            $row = $result -> fetch_all(MYSQLI_ASSOC);

            $list = $this->getUserName($row);

            return $list;
        }

         private function getUserName($list){
            $db = $this->databaseConnection();

            $sql = 'SELECT id, name FROM user';
            $result = $db -> query($sql);

            $i = 0;

            $row = $result -> fetch_all(MYSQLI_ASSOC);

            foreach($list as $image){
                foreach($row as $user){
                    if($image['user'] === $user['id']){
                        $list[$i]['user'] = $user['name'];
                        $i = $i + 1;
                    }
                }
            }


            return $list;
        }

    }

?>