<?php

    include('boot/databaseconnection.php');

    class MessageService extends Db{

        public function allMessage(){
            $db = $this->databaseConnection();

            $id = $_SESSION['userData']['id'];

            $sql = 'SELECT address, title, createdAt, description FROM message where user = "'.$id.'"';
            $result = $db -> query($sql);

            $row = $result -> fetch_all(MYSQLI_ASSOC);

            return $row;
        }


        public function validMessage($address, $title, $description, $userId){
            
            $isError = false;

            if(empty($address)){
                ErrorManager::setError('addressEmpty', 'Address is empty!');
                $isError = true;
            }

            if(empty($title)){
                ErrorManager::setError('titleEmpty', 'Title is empty!');
                $isError = true;
            }

            if(empty($description)){
                ErrorManager::setError('descriptionEmpty', 'Description is empty!');
                $isError = true;
            }

            if($isError == true){
                return false;
            }

            $inserV = $this->insertMessage($address, $title, $description, $userId);

            if($inserV === false){
                ErrorManager::setError('insertError', 'Insert error!');
                return false;
            }
            
            return true;
        }


        private function insertMessage($address, $title, $description, $userId){

            $date = date("Y-m-d h:i:s");

            $db = $this->databaseConnection();

            $sqlE = 'SELECT id FROM user WHERE email = "'.$address.'"';
            $result = $db -> query($sqlE);

            $row = $result -> fetch_all(MYSQLI_ASSOC);

            $id = intval($row[0]['id']);

            $sql = 'INSERT INTO message (user,address, title, description, createdAt, updatedAt) VALUES ('.$id.', "'. $_SESSION['userData']['email'].'", "'.$title.'", "'.$description.'", "'.$date.'", "'.$date.'")';
            
            if (mysqli_query($db, $sql)) {

            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($db);
                return false;
            }

            return true;

        }

    }

?>