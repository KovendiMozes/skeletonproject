<?php

    include('boot/databaseconnection.php');

    class RegisterService extends Db{

        public function registerValid($email, $name, $psw, $rpsw){
            
            $isError = false;

            if(empty($email)){
                ErrorManager::setError('emailEmpty', 'Email is empty!');
                $isError = true;
            }

            if(empty($name)){
                ErrorManager::setError('nameEmpty', 'Username is empty!');
                $isError = true;
            }

            if(empty($psw)){
                ErrorManager::setError('pswEmpty', 'Password is empty!');
                $isError = true;
            }

            if(empty($rpsw)){
                ErrorManager::setError('rpswEmpty', 'Confirm password is empty!');
                $isError = true;
            }

            if($psw != $rpsw){
                ErrorManager::setError('equalsEmpty', 'Password and confirm password is not equals!');
                $isError = true;
            }

            if($isError == true){
                return false;
            }

            $insertV = $this->insertValid($email, $name, $psw);

            if(!$insertV){
                ErrorManager::setError('insertError', 'Insert Error!');
                return false;
            }

            return true;

        }


        private function insertValid($email, $name, $psw){
            $db = $this->databaseConnection();

            $date = date("Y-m-d h:i:s");

            $rule = 'felhasznalo';

            $password = md5($psw);

            $sql = 'INSERT INTO user (name, email, password, rule, createdAt, updatedAt) VALUES ("'.$name.'","'.$email.'","'.$password.'","'.$rule.'","'.$date.'","'.$date.'")';
            
            if (mysqli_query($db, $sql)) {

            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($db);
                return false;
            }

            return true;
        }

    }

?>