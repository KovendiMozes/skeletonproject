<?php

include('boot/databaseconnection.php');

    class UploadImage extends Db{

        public function uploadValidationService($title, $description, $event, $whoCanSee, $file, $userId, $permission){
            $isError = false;

            if(empty($description)){
                ErrorManager::setError('descriptionEmpty', 'Description is empty!');
                $isError = true;
            }

            if(empty($event)){
                ErrorManager::setError('eventEmpty', 'Event is empty!');
                $isError = true;
            }

            if(empty($whoCanSee)){
                ErrorManager::setError('whoCanSeeEmpty', 'Who can see is empty!');
                $isError = true;
            }

            if(empty($title)){
                ErrorManager::setError('titleEmpty', 'Title is empty!');
                $isError = true;
            }

            if(!$whoCanSee == "ALL" || !$whoCanSee == 'PR' || !$whoCanSee == 'NOBODY'){
                ErrorManager::setError('badContentEmpty', 'Bad content! Suggeston content: ALL, PR or NOBODY');
                $isError = true;
            }

            if($isError == true){
                return false;
            }

            if(!isset($permission)){
                $iPermission = 'NO';
            }
            else{
                $iPermission = 'YES';
            }

            $eventV = $this->eventValdation($event);

            if($eventV === false){
                ErrorManager::setError('eventError', 'Event error!');
                return false;
            }

            $fileV = $this->fileValidation($file);

            if(!$fileV){
                return false;
            }

            $fileName = $file['name'];

            $uploadD = $this->uploadDatebase($title, $description, $event, $whoCanSee, $userId, $permission, $fileName);

            if($uploadD === false){
                ErrorManager::setError('insertError', 'Insert error!');
                return false;
            }

            return true;

        }

        private function uploadDatebase($title, $description, $event, $whoCanSee, $userId, $permission, $fileName){
            $db = $this->databaseConnection();

            $date = date("Y-m-d h:i:s");
            $id = intval($userId);

            $sql = 'INSERT INTO uploadImage (name, title, description, event, user, whoCanSee, permission, createdAt, updatedAt) VALUES ("'.$fileName.'", "'.$title.'", "'.$description.'", "'.$event.'", '.$id.', "'.$whoCanSee.'", "'.$permission.'", "'.$date.'", "'.$date.'")';
            
            if (mysqli_query($db, $sql)) {

            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($db);
                return false;
            }

            return true;
        }

        private function fileValidation($file){
            if(isset($file)){

                $fileName = $file['name'];
                $fileTmpName = $file['tmp_name'];
                $fileSize = $file['size'];
                $fileError = $file['error'];
                $fileType = $file['type'];


                $fileExt = explode('.', $fileName);
                $fileActualiExt = strtolower(end($fileExt));

                $allowed = array('jpg', 'jpeg', 'png');

                if(in_array($fileActualiExt, $allowed)){
                    if($fileError === 0){
                        if($fileSize < 10000000){
                            $fileDestination = 'image/'.$fileName;
                            move_uploaded_file($fileTmpName, $fileDestination);
                        }
                        else{
                            ErrorManager::setError('imageError', 'Wrong size!');
                            return false;
                        }
                    }
                    else{
                        ErrorManager::setError('imageError', 'Uploading error!');
                        return false;
                    }
                }
                else{
                    ErrorManager::setError('imageError', 'Wrong file type!');
                    return false;
                }

                return true;

            }
        }


        private function eventValdation($event){
            $db = $this->databaseConnection();

            $sql = 'SELECT name FROM event';
            $result = $db -> query($sql);

            $row = $result -> fetch_all(MYSQLI_ASSOC);

            foreach($row as $ev){
                if($ev['name'] === $event){
                    return true;
                }
            }

            return false;
        }



    }

?>