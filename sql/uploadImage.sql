-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 19, 2020 at 07:37 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ZProject`
--

-- --------------------------------------------------------

--
-- Table structure for table `uploadImage`
--

CREATE TABLE `uploadImage` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `event` varchar(255) NOT NULL,
  `user` int(11) NOT NULL,
  `whoCanSee` varchar(255) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `uploadImage`
--

INSERT INTO `uploadImage` (`id`, `name`, `title`, `description`, `event`, `user`, `whoCanSee`, `permission`, `createdAt`, `updatedAt`) VALUES
(2, 'Screenshot from 2020-06-18 17-48-46.png', 'Screen', 'Screen kep', 'Screen', 1, 'ALL', 'on', '2020-06-18 10:47:14', '2020-06-18 10:47:14'),
(3, 'Screenshot from 2020-06-17 17-59-53.png', 'Screen', 'egy kepernyo kep', 'Screen', 1, 'ALL', 'on', '2020-06-18 11:56:33', '2020-06-18 11:56:33'),
(4, 'Screenshot from 2020-06-11 12-05-37.png', 'Cim', 'egy kepernyo kep', 'Screen', 1, 'ALL', 'on', '2020-06-19 12:59:29', '2020-06-19 12:59:29'),
(5, '109676.jpg', 'Cim', 'egy kepernyo kep', 'Background', 2, 'ALL', 'on', '2020-06-19 01:48:02', '2020-06-19 01:48:02'),
(6, '8gs2z2p.png', 'Cim', 'egy kepernyo kep', 'Background', 4, 'ALL', 'on', '2020-06-19 07:35:24', '2020-06-19 07:35:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `uploadImage`
--
ALTER TABLE `uploadImage`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `uploadImage`
--
ALTER TABLE `uploadImage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
