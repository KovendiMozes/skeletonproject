<div class="rowEvent">
    <h2>Event</h2>
    <?php
         if(ErrorManager::isError()){
            ErrorManager::getErrors();
            ErrorManager::reset();
        };
    ?>
    <form action="<?= $this->baseUrl ?>/event" method="POST" enctype="multipart/form-data">
        <div class="container">

            <label for="name"><b>Event name</b></label>
            <input type="text" placeholder="Enter Event Name" name="name">

            <div class="action">
                <button type="" id="uploadButton">Upload</button>
            </div>
        </div>
    </form>
</div>

<div class="footerUpload">
  <h2>kovendi.mozes@gmail.com</h2>
</div>


