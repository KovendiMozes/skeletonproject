<!DOCTYPE html>
<html lang="en">
<head>
<title><?= $this->title; ?></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?= $this->baseUrl."/css/main.css" ?>">
</head>
<body>

<div class="header">
  <h1>ZProject</h1>
</div>

<div class="navbar">
  <a href="<?= $this->baseUrl ?>/home">Home</a>
  <?php if(Auth::isGuest() == false){ ?>
    <a href="<?= $this->baseUrl ?>/user">User</a>
    <a href="<?= $this->baseUrl ?>/upload">Upload</a>
    <?php if(Auth::isPR() == true){ ?>
    <a href="<?= $this->baseUrl ?>/event">Event</a> 
    <?php }?>
    <a href="<?= $this->baseUrl ?>/login" class="right">Logout</a> 
    <a href="<?= $this->baseUrl ?>/profile" class="right">Profile</a> 
    <a href="<?= $this->baseUrl ?>/message" class="right">Message</a> 
  <?php } else {?>
    <a href="<?= $this->baseUrl ?>/login" class="right">Login</a> 
  <?php }?>
</div>

<?php include($content) ?>

</body>
</html>
