<!DOCTYPE html>
<html>
<head>
<title><?= $this->title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?= $this->baseUrl."/css/login.css" ?>">
<style>

</style>
</head>
<body>

<div class="row">
    <h2>Login</h2>
    <?php
         if(ErrorManager::isError()){
            ErrorManager::getErrors();
            ErrorManager::reset();
        };
    ?>
    <form action="<?= $this->baseUrl ?>/login" method="POST">
        <div class="container">
            <label for="uname"><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="uname">

            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="psw">
            <div class="action">
                <button type="" class="login">Login</button>
                <a href="<?= $this->baseUrl ?>/register" class="register">Register</a>
            </div>
        </div>
    </form>
</div>
</body>
</html>
