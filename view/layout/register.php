<!DOCTYPE html>
<html>
<head>
<title><?= $this->title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?= $this->baseUrl."/css/register.css" ?>">
<style>

</style>
</head>
<body>

<div class="row">
    <h2>Registration</h2>
    <?php
         if(ErrorManager::isError()){
            ErrorManager::getErrors();
            ErrorManager::reset();
        };
    ?>
    <form action="<?= $this->baseUrl ?>/register" method="POST">
        <div class="container">

            <label for="email"><b>Email</b></label>
            <input type="text" placeholder="Enter Email" name="email" >

            <label for="uname"><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="uname" >

            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="psw" >
            
            <label for="rpsw"><b>Confirm Password</b></label>
            <input type="password" placeholder="Confirm Password" name="rpsw" >

            <div class="action">
                <button type="" class="submit">Submit</button>
                <a href="<?= $this->baseUrl ?>/login" class="back">Back</a>
            </div>
        </div>
    </form>
</div>
</body>
</html>
