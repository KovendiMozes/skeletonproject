<div class="rowSendMessage">
    <h2>Send Message</h2>
    <?php
         if(ErrorManager::isError()){
            ErrorManager::getErrors();
            ErrorManager::reset();
        };
    ?>
    <form action="<?= $this->baseUrl ?>/message" method="POST">
        <div class="container">

            <label for="address"><b>Addressee</b></label>
            <input type="text" placeholder="Enter Addressee" name="address" >

            <label for="title"><b>Title Message</b></label>
            <input type="text" placeholder="Enter Title" name="title" >

            <label for="description"><b>Description</b></label>
            <input type="text" placeholder="Enter Description" name="description" >

            <div class="actionMessage">
                <button type="" id="sendButton">Send</button>
            </div>
        </div>
    </form>
</div>
</div>  
</div>

<div class="footer">
  <h2>kovendi.mozes@gmail.com</h2>
</div>