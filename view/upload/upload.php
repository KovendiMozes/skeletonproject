<div class="rowUpload">
    <h2>Upload</h2>
    <?php
         if(ErrorManager::isError()){
            ErrorManager::getErrors();
            ErrorManager::reset();
        };
    ?>
    <form action="<?= $this->baseUrl ?>/upload" method="POST" enctype="multipart/form-data">
        <div class="container">

            <label for="title"><b>Title Image</b></label>
            <input type="text" placeholder="Enter Title" name="title">

            <label for="description"><b>Description</b></label>
            <input type="text" placeholder="Enter Description" name="description">

            <label for="event"><b>Event</b></label>
            <input type="text" placeholder="Enter Event" name="event">

            <label for="whoCanSee"><b>Who Can See</b></label>
            <input type="text" placeholder="ALL, PR or NOBODY" name="whoCanSee">

            <input type="file" name="image" >
            <br>
            <br>
            <input type="checkbox" id="permission" name="permission">
            <label for="permission"> Enabled </label><br>

            <div class="action">
                <button type="" id="uploadButton">Upload</button>
            </div>
        </div>
    </form>
</div>

<div class="footerUpload">
  <h2>kovendi.mozes@gmail.com</h2>
</div>


